--liquibase formatted sql

--changeset public:1
create table test_33 (
    id int primary key,
    name varchar(255)
);

--changeset public:2
insert into test_33 (id, name) values (1, 'name222ddd' );
insert into test_33 (id, name) values (2, 'name222ddd' );

--rollback drop table test_33;
