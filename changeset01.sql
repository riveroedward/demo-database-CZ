--liquibase formatted sql

--changeset public:1
create table test1 (
    id int primary key,
    name varchar(255)
);
--rollback drop table test1;

--changeset public:2
insert into test1 (id, name) values (1, 'name' );
insert into test1 (id, name) values (2, 'name' );

--rollback DELETE FROM TEST1;